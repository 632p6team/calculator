﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    interface IArithmeticOperations
    {
        // Значение в кавычках - как функция отобразится в строке которую вводит пользователь

        // Приведение целого числа к дробному
        double castIntToDouble(int number);

        // Сложение чисел "+"
        double sum(double firstNumber, double secondNumber);

        /**
         * Разность чисел
         * decreasing - уменьшаемое
         * negative - вычитаемое
         * "-"
         */
        double difference(double decreasing, double negative);

        // Умножение "*"
        double multiplication(double firstNumber, double secondNumber);

        // Деление "/"
        double division(double firstNumber, double secondNumber);

        // Вычисление N - го количества процентов от числа "%"
        double percentages(double sourceNumber, int percentagesAmount);

        // Извлечение квадратного корня "sqrt[]"
        double sqrt(double number);

        // Возведение в квадрат "^2"
        double sqr(double number);

        // Возведение в степень "^5"
        double power(double number, int degree);

        // Приведение числа к знаменателю ( 1/x )
        double denominator(double number);

        // Получение числа пи "П"
        double getPi();

        // Модуль числа "abs[-10]"
        double abs(double number);

        // ln - вычисление натурального логорифма числа "ln[1]"
        double ln(double number);

        // lg - вычисление логорифма числа "lg[1]"
        double lg(double number);

        // вычисление синуса числа "sin[1]"
        double sin(double number);

        // вычисление арксинуса числа "asin[1]"
        double asin(double number);

        // вычисление косинуса числа "cos[1]"
        double cos(double number);

        // вычисление арккосинуса числа "acos[1]"
        double acos(double number);

        // вычисление тангенса числа "tg[1]"
        double tg(double number);

        // вычисление арктангенса числа "atg[1]"
        double atg(double number);

        // вычисление котангенса числа "ctg[1]"
        double сtg(double number);

        // вычисление арккотангенса числа "actg[1]"
        double actg(double number);

        /**
         * Округление числа
         * [20 , 20.4999...] == 20
         * [20.5 , 20.999...] == 21
         */
        int roundANumber(double number);

    }
}
