﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class CArithmeticOperations : IArithmeticOperations
    {
        public double abs(double number)
        {
            return Math.Abs(number);
        }

        public double acos(double number)
        {
            return Math.Acos(number);
        }

        public double actg(double number)
        {
            throw new NotImplementedException();
        }

        public double asin(double number)
        {
            return Math.Asin(number);
        }

        public double atg(double number)
        {
            return Math.Atan(number);
        }

        public double castIntToDouble(int number)
        {
            return number;
        }

        public double cos(double number)
        {
            return Math.Cos(number);
        }

        public double denominator(double number)
        {
            return (1 / number);
        }

        public double difference(double decreasing, double negative)
        {
            return (decreasing - negative);
        }

        public double division(double firstNumber, double secondNumber)
        {
            return (firstNumber / secondNumber);
        }

        public double getPi()
        {
            return Math.PI;
        }

        public double lg(double number)
        {
            throw new NotImplementedException();
        }

        public double ln(double number)
        {
            throw new NotImplementedException();
        }

        public double multiplication(double firstNumber, double secondNumber)
        {
            return (firstNumber * secondNumber);
        }

        public double percentages(double sourceNumber, int percentagesAmount)
        {
            throw new NotImplementedException();
        }

        public double power(double number, int degree)
        {
            return (Math.Pow(number, degree));
        }

        public int roundANumber(double number)
        {
            throw new NotImplementedException();
        }

        public double sin(double number)
        {
            return Math.Sin(number);
        }

        public double sqr(double number)
        {
            return (number * number);
        }

        public double sqrt(double number)
        {
            return Math.Sqrt(number);
        }

        public double sum(double firstNumber, double secondNumber)
        {
            return (firstNumber + secondNumber);
        }

        public double tg(double number)
        {
            return Math.Tan(number);
        }

        public double сtg(double number)
        {
            throw new NotImplementedException();
        }
    }
}
