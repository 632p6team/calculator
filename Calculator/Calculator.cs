﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public interface ICalculator {     
        /**
         * Текущяя историй операций
         * Каждый обьект - строка, содержит все вводимые пользователем операции до нажатия кнопки "равно", пробел, символ " = ", пробел и результат
         * Должна отображаться на экране в процессе работы приложения
         */
        //List<String> history = null;

        /**
         * Если файл истории отстутствует - создает новый файл history.txt и записывает в него содержимое переменной  List<String> history
         * Если файл истории создан - дописывает в него содержимое переменной  List<String> history
         * Каждая новая строка файла содержит обьект типа String и символ перевода строки
         * @throws IOException
         * В случаии возникновения ошибки IOException при работе с файлом - выводит пользователю уведомление "Не удалось сохранить историю операций"
         * Вызывать перед закрытием программы
         */
        void historyToFile();

        /**
         * Считывает в переменную List<String> history сожержимое файла history.txt если он существует и не пуст
         * В случаии возникновения ошибки IOException при работе с файлом - действия не требуются"
         */
        void historyFromFile();

        /**
         * Парсит строку и производит вычисления используя методы класса ArithmeticOperations
         * Пример входных данных:
         * "100+2  - 5 *9 *( abc[ - 9.333]/ 3)* sin[2]/arctg[1]"
         * Ожидаемый результат:
         * 101.89142767
         * @param input принимает строку с дупостимыми символами
         * @return double результат
         * @throws IllegalArgumentException
         * В случии неправильного синтаксиса исходной строки и возникновения ошибок - вывести уведомление "неправильны ввод!"
         */
        double count(String input);
    }

    class Calculator : ICalculator
    {
        public double count(string input)
        {
            throw new NotImplementedException();
        }

        public void historyFromFile()
        {
            throw new NotImplementedException();
        }

        public void historyToFile()
        {
            throw new NotImplementedException();
        }
    }

}
